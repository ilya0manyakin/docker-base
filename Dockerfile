# use the ubuntu release
FROM ubuntu:20.04

RUN apt update
RUN apt install -y software-properties-common
RUN apt-get install -y python3 python3-pip python3-virtualenv
# list modules
RUN apt list --installed | grep python

# go to user directory
RUN mkdir -p /home/root
RUN cd /home/root

# make virtualenv 
RUN python3 -m virtualenv /home/root/virtualenv


